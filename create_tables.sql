Create the Films table
CREATE TABLE netflix.Films (
-- Create the Movies table
CREATE TABLE Movies (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  creator VARCHAR(255),
  type VARCHAR(255),
  imdb_rank DECIMAL(3, 1),
  release_date DATE,
  genre VARCHAR(255)
);


-- Create the MoviesData table
CREATE TABLE MoviesData (
  movie_id INT PRIMARY KEY,
  url VARCHAR(255),
  FOREIGN KEY (movie_id) REFERENCES Movies(id)
);

-- Create the Comments table
CREATE TABLE Comments (
  id INT PRIMARY KEY,
  movie_id INT,
  comment_text VARCHAR(255),
  FOREIGN KEY (movie_id) REFERENCES Movies(id)
);

-- Create the PreviewImage table
CREATE TABLE PreviewImage (
  id INT PRIMARY KEY,
  movie_id INT,
  image_url VARCHAR(255),
  FOREIGN KEY (movie_id) REFERENCES Movies(id)
);

-- Create the Users table
CREATE TABLE Users (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  age INT,
  birth_date INT,
  subscription VARCHAR(255)
);

-- Create the UserStars table
CREATE TABLE UserStars (
  user_id INT,
  movie_id INT,
  datetime TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES Users(id),
  FOREIGN KEY (movie_id) REFERENCES Movies(id)
);

-- Insert data into the Movies table
INSERT INTO Movies (id, name, creator, type, imdb_rank, release_date, genre)
VALUES (1, 'Kill Bill', 'Quentin Tarantino', 'Movie', 7.8, '2003-10-10', 'Action, Crime');

INSERT INTO Movies (id, name, creator, type, imdb_rank, release_date, genre)
VALUES (2, 'The Queen''s Gambit', 'Scott Frank', 'Series', 8.6, '2020-10-23', 'Drama');

-- Insert data into the MoviesData table
INSERT INTO MoviesData (movie_id, url)
VALUES (1, 'https://s3.example.com/kill_bill.mp4');

-- Insert data into the Comments table
INSERT INTO Comments (id, movie_id, comment_text)
VALUES (1, 1, 'Great movie!');

-- Insert data into the PreviewImage table
INSERT INTO PreviewImage (id, movie_id, image_url)
VALUES (1, 1, 'https://s3.example.com/kill_bill_poster.jpg');

-- Insert data into the Users table
INSERT INTO Users (id, name, age, birth_date, subscription)
VALUES (1, 'John Doe', 30, 1993, 'VIP');

-- Insert data into the UserStars table
INSERT INTO UserStars (user_id, movie_id, datetime)
VALUES (1, 1, '2023-07-19 10:30:00');
